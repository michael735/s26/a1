//1. What directive is used by Node.js in loading the modules it needs?

    //npm init
    //npm install nodemon

//2. What Node.js module contains a method for server creation?

    //HTTP module

//3. What is the method of the http object responsible for creating a server using Node.js?

    //HTTP.createServer(request, response)

//4. What method of the response object allows us to set status codes and content types?

    //response.writeHead()

//5. Where will console.log() output its contents when run in Node.js?

    //response.write()

//6. What property of the request object contains the address' endpoint?

    //.listen()

    const HTTP = require('http')
    const { Http2ServerRequest } = require('http2')

    HTTP.createServer((request, response) => {

        if(request.url === "/login"){
            response.writeHead(200, {"Content-Type" : "text/plain"});
            response.write("Welcome to the login page");
            response.end()
    
        } else {
            response.writeHead(400, {"Content-Type" : "text/plain"});
            response.write("I'm sorry the page you are looking for cannot be found");
            response.end()
        }
    }).listen(3000);
